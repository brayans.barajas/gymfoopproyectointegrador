/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package co.edu.unac.gymfoop;
import java.sql.Connection;
import java.sql.DriverManager;

import org.sqlite.JDBC;

/**
 *
 * @author braya
 */
public class Conexion {
    Connection conexion = null;
    public Conexion(){
    
    }
    
    public Connection conectar(){
        try {
            Class.forName("org.sqlite.JDBC");
            conexion = DriverManager.getConnection("jdbc:sqlite:bryanmi.db");
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        return conexion;
    } 
}
